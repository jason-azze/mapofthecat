# Map of the Cat
## A process to generate an annotated table of contents for a directory.

> I began to read the paper. It kept talking about extensors and flexors, the gastrocnemius muscle, and so on. This and that muscle were named, but I had not the foggiest idea of where they were located in relation to the nerves or to the cat. So I went to the librarian in the biology section and asked her if she could find me a map of the cat.<br>
> "A map of the cat, sir?" she asked horrified. "You mean a zoological chart!" From then on there were rumors about a dumb biology student who was looking for "a map of the cat."<br>
> --Richard P. Feynman, "A Map of the Cat?", Surely You're Joking, Mr. Feynman. W.W. Norton, 1985, p.72

I joined a new software project. They had a repositiory with a bunch of files and directories -- as one does. How could I get up to speed on what files did what? I generated a directory listing, opened it in my text editor, and started annotating each line. "I should include my notes with the repo!", I thought. But, as soon as you include such a document it becomes stale. "I should autogenerate these notes from annotations included in each file as comments!" Now we're talking!

This is just an amoeba. Just a start. A PoC. It wants to be a self-contained program. Right now it's a pipeline of other peoples stuff.

### The story so far

`testfile.txt` contains a comment with an arbitrary (hopefully unique in your project) delimeter followed by some JSON.
```
# A comment with a delimiter
# MAPCAT {"Filename":"testfile.txt","Layer":"meta","Notes":"A file for testing the function of mapcat"}
Then we have some text that is not part of a comment.
``` 
`testfile2.txt` is similar.
```
# A comment with a delimiter
# MAPCAT {"Filename":"testfile2.txt","Layer":"meta","Notes":"Another file for testing the function of mapcat"}
Then we have some text that is not part of a comment.
```

The goal is to walk through all of the files, find the special comments, and generate a markdown table from them.
| Filename      | Layer | Notes                                           |
| ------------- | ----- | ----------------------------------------------- |
| testfile2.txt | meta  | Another file for testing the function of mapcat |
| testfile.txt  | meta  | A file for testing the function of mapcat       |

### Kludgy Proof of Concept

I've just taped together some Unix tools and citycide's [tablemark-cli](https://github.com/citycide/tablemark-cli) as a PoC.
These are the steps I used to generate a table on my Ubuntu machine.

#### Install Node and tablemark-cli
```
# Install nvm Node Version Manager
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh > install.sh
# Read the install.sh to be sure you trust what it is doing.
chmod 755 install.sh 
./install.sh 
# There are several ways to refresh your environment. This is my favorite.
exec bash
# Did it install? This should return nvm
command -v nvm
# Install the latest version of node.
nvm install node
# Update npm
nvm install-latest-npm
# Use npm to install yarn. You can use npm instead of yarn if you prefer.
npm install -g yarn
# Install tablemark-cli
yarn global add tablemark-cli
# Fix tablemark-cli's bad line breaks
sudo apt install dos2unix
dos2unix ~/.config/yarn/global/node_modules/tablemark-cli/cli.js #Fix bad line breaks
# Check the installation. Should return the version number
~/.yarn/bin/tablemark -v
```

#### Annotate some files

Given in a file testfile.txt
```
# MAPCAT {"Filename":"testfile.txt","Layer":"meta","Notes":"A file for testing the function of mapcat"}
```
and a file testfile2.txt which contains
```
# MAPCAT {"Filename":"testfile2.txt","Layer":"meta","Notes":"Another file for testing the function of mapcat"}
```

#### Glue it all together
```
# Walk through our files looking for our magic delimeter and collect the output.
grep "MAPCAT " * |awk -F'MAPCAT ' '{print $2}' > output.json
# Convert the output to a Markdown table.
~/.yarn/bin/tablemark output.json > table.md
```
