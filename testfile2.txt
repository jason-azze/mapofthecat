# A comment with a delimiter
# MAPCAT {"Filename":"testfile2.txt","Layer":"meta","Notes":"Another file for testing the function of mapcat"}
Then we have some text that is not part of a comment.
How about some "quotes" and 'single quotes'?
Then another comment below.
# Here is that comment
