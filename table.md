| Filename      | Layer | Notes                                           |
| ------------- | ----- | ----------------------------------------------- |
| testfile2.txt | meta  | Another file for testing the function of mapcat |
| testfile.txt  | meta  | A file for testing the function of mapcat       |

